const express = require("express")
const app = express()
const dotenv = require("dotenv")
const mongoose = require("mongoose")
const cors = require("cors")

const PORT = process.env.PORT || 3000

// Initialize Environment Variables
dotenv.config();

// Default Route
app.get("/", (req, res) => {
    res.send("I am working")
})

// Listent to port
app.listen(PORT, () => console.log(`Listening on port ${PORT}`))

// Import Routes
const authRoute = require("./routes/auth/auth")
const authDashboard = require("./routes/auth/authDashboard")

// Connect to Database
mongoose.connect(process.env.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).finally(() => {
    console.log("Connected to Database")
})

// Specify middleware (disable cors and use json output)
app.use(express.json(), cors())

// Route middleware
app.use("/api/users", authRoute)
app.use("/api/dashboard", authDashboard)