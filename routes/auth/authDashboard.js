const User = require("../../model/User")
const verify = require("./authVerify")

const router = require("express").Router()

router.get("/getUsers", verify, async (_req, res) => {
    try {
        const results = await User.findOne().exec()
        res.send(results)
    }
    catch (err) {
        res.status(500).send(err.message)
    }
})

module.exports = router