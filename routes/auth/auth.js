const router = require('express').Router()
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const User = require('../model/User')

// User input validation
const Joi = require('joi')
const registerSchema = Joi.object({
    fname: Joi.string().min(3).required(),
    lname: Joi.string().min(3).required(),
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required()
})

// User Signup
router.post('/register', async (req, res) => {

    // Check if email already exists
    const emailExist = await User.findOne({ email: req.body.email })
    if (emailExist) {
        return res.status(400).json({ error: 'Email already exists' })
    }

    // Hash password
    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await bcrypt.hash(req.body.password, salt).catch(err => {
        return res.status(500).json({ error: err.message })
    })

    // Create new user
    const user = new User({
        fname: req.body.fname,
        lname: req.body.lname,
        email: req.body.email,
        password: hashedPassword
    })

    // Save user
    try {
        // Validate User Inputs
        const { error } = registerSchema.validateAsync(req.body)

        // Respond with error messge if validation failed
        if (error) {
            return res.status(400).json({ error: error.details[0].message })
        }

        // Save user
        const savedUser = await user.save()
        res.status(201).json(savedUser)
    } catch (error) {
        // Respond if any other error occurs
        res.status(500).json(error)
    }
})

// Login Schema
const loginSchema = Joi.object({
    email: Joi.string().min(6).required().email(),
    password: Joi.string().min(6).required()
})

// User Login
router.post('/login', async (req, res) => {
    
    // Verify that email is valid
    const user = await User.findOne({ email: req.body.email })
    if (!user) {
        return res.status(400).json({ error: 'Invalid Email/Password' })
    }

    // Validate Password
    const validPassword = await bcrypt.compare(req.body.password, user.password)
    if (!validPassword) {
        return res.status(400).json({ error: 'Invalid Email/Password' })
    }

    // Validate User Inputs
    try {
        const { error } = await loginSchema.validateAsync(req.body)
        if (error) {
            return res.status(400).json({ error: error.details[0].message })
        }
        else {
            // Return a token
            const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET, { expiresIn: '1h' })
            localStorage.setItem('token', token)
            res.header("auth-token", token).send(token)
        }
    }
    catch (error) {
        res.status(500).json({ error: error })
    }
})

module.exports = router