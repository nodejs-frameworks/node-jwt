set -e

mongosh <<EOF
use $MONGO_INITDB_DATABASE

db.createUser({
    user: '$MONGO_INITDB_USER',
    pwd: '$MONGO_INITDB_ROOT_PASSWORD',
    roles: [ { role: 'root', db: 'admin' } ]
})
EOF